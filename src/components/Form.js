import styled from "styled-components";
import ColorButtons from "./ColorButtons";

const Form = ({ setColor, setName, setOffice, setNumber }) => {
  const inputHandle = (event) => {
    if (event.target.name === "name") {
      setName(event.target.value);
    }
    if (event.target.name === "office") {
      setOffice(event.target.value);
    }
    if (event.target.name === "number") {
      setNumber(event.target.value);
    }
  };

  return (
    <Main>
      <input onChange={inputHandle} name="name" placeholder="nome" />
      <input onChange={inputHandle} name="office" placeholder="cargo" />
      <input onChange={inputHandle} name="number" placeholder="número" />
    </Main>
  );
};

const Main = styled.div`
  display: flex;
  gap: 5px;
  flex-direction: column;
`;

export default Form;
