import { useState } from "react";
import styled from "styled-components";
import {
  azul,
  preto,
  amarelo,
  rosa,
  violeta,
  verde,
  salmao,
  laranja,
} from "../utils/colors";

const CopyButton = ({ color }) => {
  const [message, setMessage] = useState("Copiar assinatura");

  const copyElement = () => {
    window.getSelection().removeAllRanges();
    let range = document.createRange();
    range.selectNode(document.getElementById("signature"));
    window.getSelection().addRange(range);
    document.execCommand("copy");
    window.getSelection().removeAllRanges();
    setMessage("Assinatura copiada!");
    setTimeout(() => {
      setMessage("Copiar assinatura");
    }, 3000);
  };

  return (
    <Main color={color} onClick={copyElement}>
      {message}
    </Main>
  );
};

const Main = styled.button`
  padding: 8px 0px;
  width: 200px;
  border: none;
  color: ${(props) =>
    props.color === "amarelo" || props.color === "verde"
      ? "rgba(0, 0, 0, 0.8)"
      : "rgba(255, 255, 255, 0.8)"};
  font-size: 14px;
  background-color: ${(props) => {
    switch (props.color) {
      case "violeta":
        return violeta;
      case "rosa":
        return rosa;
      case "azul":
        return azul;
      case "salmao":
        return salmao;
      case "amarelo":
        return amarelo;
      case "verde":
        return verde;
      case "preto":
        return preto;
      case "laranja":
        return laranja;
    }
  }};
  border-radius: 3px;
  transition: 500ms;
  font-weight: 900;
  :hover {
    cursor: pointer;
    color: ${(props) =>
      props.color === "amarelo" || props.color === "verde" ? "black" : "white"};
    width: 225px;
  }
`;

export default CopyButton;
