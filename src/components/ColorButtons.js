import styled from "styled-components";
import {
  azul,
  preto,
  amarelo,
  rosa,
  violeta,
  verde,
  salmao,
  laranja,
} from "../utils/colors";

const ColorButtons = ({ setColor }) => {
  return (
    <Main>
      <Button
        onClick={() => {
          setColor("violeta");
        }}
        color="violeta"
      />
      <Button
        onClick={() => {
          setColor("rosa");
        }}
        color="rosa"
      />
      <Button
        onClick={() => {
          setColor("salmao");
        }}
        color="salmao"
      />
      <Button
        onClick={() => {
          setColor("verde");
        }}
        color="verde"
      />
      <Button
        onClick={() => {
          setColor("azul");
        }}
        color="azul"
      />
      <Button
        onClick={() => {
          setColor("laranja");
        }}
        color="laranja"
      />
      <Button
        onClick={() => {
          setColor("amarelo");
        }}
        color="amarelo"
      />
      <Button
        onClick={() => {
          setColor("preto");
        }}
        color="preto"
      />
    </Main>
  );
};

const Main = styled.div`
  display: flex;
  gap: 10px;
`;

const Button = styled.button`
  border: none;
  border-radius: 5px;
  height: 25px;
  width: 25px;
  background-color: ${(props) => {
    switch (props.color) {
      case "violeta":
        return violeta;
      case "rosa":
        return rosa;
      case "azul":
        return azul;
      case "salmao":
        return salmao;
      case "amarelo":
        return amarelo;
      case "verde":
        return verde;
      case "preto":
        return preto;
      case "laranja":
        return laranja;
    }
  }};
`;

export default ColorButtons;
