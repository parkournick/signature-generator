import amarelo from "../assets/logos/amarelo.png";
import azul from "../assets/logos/azul.png";
import laranja from "../assets/logos/laranja.png";
import preto from "../assets/logos/preto.png";
import rosa from "../assets/logos/rosa.png";
import salmao from "../assets/logos/salmao.png";
import verde from "../assets/logos/verde.png";
import violeta from "../assets/logos/violeta.png";

import logoLinkedin from "../assets/linkedin.png";
import logoInstagram from "../assets/instagram.png";

import styled from "styled-components";

const Signature = ({ color, name, office, number }) => {
  const setColor = () => {
    switch (color) {
      case "azul":
        return azul;
      case "amarelo":
        return amarelo;
      case "laranja":
        return laranja;
      case "preto":
        return preto;
      case "rosa":
        return rosa;
      case "salmao":
        return salmao;
      case "verde":
        return verde;
      case "violeta":
        return violeta;
    }
  };

  return (
    <Main>
      <div id="signature">
        <table>
          <tr>
            <th>
              <img style={{ height: "98px" }} src={setColor()} />
            </th>
            <td>
              <b>{name || "Nicolas"}</b>
              <br />
              {office || "Desenvolvedor Júnior"}
              <br />
              {number || "+55 31 986785819"}
              <br />
              <a href="https://www.jornadayu.com/" target="_blank">
                jornadayu.com
              </a>
              <br />
              <a
                href="https://www.linkedin.com/company/jornadayu/"
                target="_blank"
              >
                <img src={logoInstagram} />
              </a>{" "}
              <a href="https://www.instagram.com/jornadayu/" target="_blank">
                <img src={logoLinkedin} />
              </a>
              <br />
            </td>
          </tr>
        </table>
      </div>
    </Main>
  );
};

const Main = styled.div`
  height: 150px;
  min-width: 300px;
  padding: 0px 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0 1.5px 3.6px rgba(0, 0, 0, 0.042),
    0 4.3px 10px rgba(0, 0, 0, 0.06), 0 10.3px 24.1px rgba(0, 0, 0, 0.078),
    0 34px 80px rgba(0, 0, 0, 0.12);
`;

export default Signature;
