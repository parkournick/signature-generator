export const violeta = "#D92ECB";
export const preto = "#222";
export const rosa = "#FE3D9C";
export const amarelo = "#FEFE3E";
export const salmao = "#FF746C";
export const laranja = "#FFA032";
export const azul = "#00C9FB";
export const verde = "#00F92C";
