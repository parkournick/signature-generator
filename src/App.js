import { useState } from "react";
import styled from "styled-components";
import ColorButtons from "./components/ColorButtons";
import CopyButton from "./components/CopyButton";
import Form from "./components/Form";
import Signature from "./components/Signature";

const App = () => {
  const [color, setColor] = useState("violeta");
  const [name, setName] = useState("Nicolas");
  const [office, setOffice] = useState("Desenvolvedor Júnior");
  const [number, setNumber] = useState("+55 31 986785819");

  return (
    <Main>
      <Form
        setColor={setColor}
        setName={setName}
        setOffice={setOffice}
        setNumber={setNumber}
      />
      <ColorButtons setColor={setColor} />
      <Signature name={name} color={color} office={office} number={number} />
      <CopyButton color={color} />
    </Main>
  );
};

const Main = styled.main`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 20px;
`;

export default App;
